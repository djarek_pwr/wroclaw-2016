import {Component} from 'angular2/core';
import {RoomNotification} from './notification';
@Component({

  // Declare the tag name in index.html to where the component attaches
  selector: 'hello-world',
  directives: [RoomNotification],
  template: `
  <div class="container">
      <div class="row">
          <div class="col-md-6 col-md-offset-3">
              <div class="login-panel panel panel-default">
                  <div class="panel-heading">
                      <h3 class="panel-title">Enter your name</h3>
                  </div>
                  <div class="panel-body">
                      <input type="text" [(ngModel)]="name" placeholder="Enter a name here">
                      <input type="text" [(ngModel)]="pattern" placeholder="Enter a pattern here">
                      <input type="text" [(ngModel)]="msg" placeholder="Enter a notification">
                  </div>
              </div>
          </div>
      </div>
  </div>
	<hr>
	<!-- conditionally display yourName -->
  <div class="col-md-6 col-md-offset-3">
  <room-notification [name]="name" [pattern]="pattern" [msg]="msg"></room-notification>
	<h1 [hidden]="!name">Hello {{name}}!</h1>
  </div>
`
})

export class HelloWorld {
  // Declaring the variable for binding with initial value
  public name: string;
  public pattern: string;
  public msg: string;
}
