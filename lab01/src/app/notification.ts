import {Component} from 'angular2/core';

@Component({

  // Declare the tag name in index.html to where the component attaches
  selector: 'room-notification',
  inputs: ['name', 'pattern', 'msg'],
  template:
  `
    <div [hidden]="name!==pattern" class="alert alert-danger">
        {{msg}}
    </div>
  `
})
export class RoomNotification {
  public name: string;
  public pattern: string;
  public msg: string;
}
